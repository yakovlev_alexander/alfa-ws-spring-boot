package com.alfa.oms.Client;

import com.alfa.oms.models.Status;
import com.alfa.oms.models.UserJson;
import com.alfa.oms.percistence.User;
import feign.Headers;
import feign.RequestLine;

public interface UserClient {
    @RequestLine("POST /addUser")
    @Headers("Content-type: application/json")
    Status addUser(UserJson user);

    @RequestLine("POST /updateUser")
    @Headers("Content-type: application/json")
    Status updateUser(User updatedUser);

    @RequestLine("POST /blockUser")
    @Headers("Content-type: application/json")
    Status blockUser(User user);

    @RequestLine("POST /findUser")
    @Headers("Content-type: application/json")
    User findUser(UserJson user);
}
