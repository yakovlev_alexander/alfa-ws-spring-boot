package com.alfa.oms.Client;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;

public class ClientBuilder {
    protected static <T> T createClient(Class<T> type, String url) {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(type, url);
    }

    public static UserClient getUserClient() {
        return createClient(UserClient.class, "http://localhost:8080");
    }
}
