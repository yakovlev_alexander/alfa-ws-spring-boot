package com.alfa.oms;

import com.alfa.oms.models.Status;
import com.alfa.oms.models.UserJson;
import com.alfa.oms.percistence.User;
import com.alfa.oms.percistence.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger("USER-STATE-CHANGE");

    @Autowired
    UserRepository repository;

    @RequestMapping(value = "addUser", method = RequestMethod.POST)
    public Status addUser(@RequestBody UserJson user) {
        User userByLogin = repository.findByLogin(user.getLogin());
        if( userByLogin != null) {
            return Status.ERROR;
        } else {
            repository.save(new User(user.getLogin(), user.getPassword(), false));
            LOGGER.info("User " + user.getLogin() + " added.");
            return Status.OK;
        }
    }

    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    public Status updateUser(@RequestBody User user) {
        User oldUser = repository.findById(user.getId());
        User userByLogin = repository.findByLogin(user.getLogin());
        if(oldUser == null || userByLogin != null) {
            return Status.ERROR;
        } else {
            repository.save(user);
            LOGGER.info("User " + user.getLogin() + " updated.");
            return Status.OK;
        }
    }

    @RequestMapping(value = "blockUser", method = RequestMethod.POST)
    public Status blockUser(@RequestBody User user) {
        User oldUser = repository.findById(user.getId());
        if(oldUser == null || oldUser.isInactive()) {
            return Status.ERROR;
        } else {
            oldUser.setInactive(true);
            repository.save(oldUser);
            LOGGER.info("User " + user.getLogin() + " blocked.");
            return Status.OK;
        }
    }

    @RequestMapping(value = "findUser", method = RequestMethod.POST)
    public User findUser(@RequestBody UserJson user) {
        return repository.findByLoginAndPassword(user.getLogin(), user.getPassword());
    }
}
