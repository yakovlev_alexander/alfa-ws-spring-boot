package com.alfa.oms.percistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "users")
@SecondaryTable(name = "inactive_users", pkJoinColumns = @PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID"))
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;

    private String password;

    @Column(table="inactive_users")
    private Boolean inactive;

    public User() {
    }

    public User(String login, String password, Boolean inactive) {
        this.login = login;
        this.password = password;
        this.inactive = inactive;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isInactive() {
        return inactive == null ? false : inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", inactive=" + isInactive() +
                '}';
    }
}
