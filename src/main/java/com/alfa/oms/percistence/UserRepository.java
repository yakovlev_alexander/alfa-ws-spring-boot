package com.alfa.oms.percistence;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findById(Long id);

    User findByLoginAndPassword(String login, String password);

    User findByLogin(String login);
}
