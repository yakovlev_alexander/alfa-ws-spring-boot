package com.alfa.oms;

import com.alfa.oms.Client.ClientBuilder;
import com.alfa.oms.Client.UserClient;
import com.alfa.oms.models.Status;
import com.alfa.oms.models.UserJson;
import com.alfa.oms.percistence.User;

public class ApplicationTest {
    public static void main(String[] args) {
        UserClient client = ClientBuilder.getUserClient();
        System.out.println("Adding user: user1:qwerty123");
        UserJson newUser = new UserJson("user1", "qwerty123");
        Status st1 = client.addUser(newUser);
        System.out.println("Status: " + st1);
        User addedUser = client.findUser(newUser);
        System.out.println("User added " + addedUser);

        System.out.println("Blocking user...");
        Status st2 = client.blockUser(addedUser);
        System.out.println("Status: " + st2);
        User blockedUser = client.findUser(newUser);
        System.out.println("User blocked " + blockedUser);

        System.out.println("Updating user...");
        blockedUser.setLogin("newLogin");
        blockedUser.setPassword("newPassword");
        Status st3 = client.updateUser(blockedUser);
        System.out.println("Status: " + st3);
        User updatedUser = client.findUser(new UserJson("newLogin", "newPassword"));
        System.out.println("Updated user " + updatedUser);
    }
}
