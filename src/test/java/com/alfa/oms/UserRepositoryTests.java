package com.alfa.oms;

import com.alfa.oms.percistence.User;
import com.alfa.oms.percistence.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Sql(value = {"classpath:createDB.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"classpath:dropDB.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTests {
	@Autowired
	UserRepository repository;

	@Test
	public void findByIdTest_exist() {
		User user = repository.findById(1L);
		assertEquals(user.getLogin(), "Titkin");
	}

	@Test
	public void findByIdTest_absent() {
		User user = repository.findById(4L);
		assertNull(user);
	}

	@Test
	public void findByLoginAndPasswordTest_exist() {
		User user = repository.findByLoginAndPassword("Titkin", "qwerty");
		assertEquals(user.getLogin(), "Titkin");
	}

	@Test
	public void findByLoginAndPasswordTest_absent() {
		User user = repository.findByLoginAndPassword("Titkin", "nopass");
		assertNull(user);
	}
}
