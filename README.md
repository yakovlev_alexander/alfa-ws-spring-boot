Simple webserver on Spring Boot for CRUD operations on User  
`Users` table has the following fields  
**Id** long  
**login** varchar(20)  
**password** varchar(10)  
  
Technology stack: Spring Boot, H2 database in memory, Maven, Log4j2, Feign client  
  
__  
Made by **_Yakovlev Alexander_**  
